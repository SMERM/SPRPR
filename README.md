# Seminario di Programmazione Professionale (SPRPR)

Materiali del *Seminario di Programmazione Professionale* tenuto da Nicola
Bernardini a partire dal 2023 (A.A.2022-2023) in seno alla Scuola di Musica
Elettronica.

# LICENZE

## Software

[![GNU GPL v.3 License](https://it.wikipedia.org/wiki/GNU_General_Public_License#/media/File:GPLv3_Logo.svg)](./LICENSE-sw)

## Documentazione

[<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Seminario di Programmazione Professionale</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/SMERM/SPRPR" rel="dct:source">https://gitlab.com/SMERM/SPRPR</a>](./LICENSE-doc)
