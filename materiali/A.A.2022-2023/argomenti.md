# Argomenti del corso (A.A.2022-2023)

## Introduzione

* ecosistema del seminario
* il mercato del software
* strumentario professionale
* linguaggi di programmazione
* tipologie di programmazione
* cicli di programmazione
* tecniche di programmazione
* interfacce utente
* *single-tier* vs. *multi-tier*
* programmazione di sistema

## Ecosistema del seminario

* *git*
* repositories individuali su *gitlab*
* progetti individuali (o di gruppo):
  * *SEC*
  * *osc*
  * *nel*
  * *spotify analyzer*
  * *freesound interface app*
  * *fortelib*
  * ...

## Il mercato del software

* software non-libero
* Software Libero

## Strumentario professionale

### *command-line interface*

#### comandi essenziali

* *ls*
* *grep*
* *cat*
* *more*/*less*
* ...

### strumenti SCM

### strumenti di sequenza (*make* e derivati)

### espressioni regolari

### strumenti di test

### linguaggi di programmazione:

#### interpretati

#### compilati

#### *byte-code* producers

## Tipologie di programmazione

### *Test-driven development*

#### *unit testing*

#### *functional testing*

### *Behaviour-driven development*

### *Fixtures* e *Mock-ups*

## Cicli di programmazione

### programmazione agile

### integrazione continua

## Tecniche di programmazione

### regole generali

### programmazione strutturata

### programmazione a oggetti

## Interfacce utente

## *Single-tier* vs. *Multi-tier* programming

### *Monolithic* programming

### *Multi-task* programming

### *Multi-thread* programming

### *Model-viev-controller* programming

## Programmazione di sistema

### programmazione di *device drivers*

### sistemi fisici vs. sistemi virtuali

### containers

### orchestrators
