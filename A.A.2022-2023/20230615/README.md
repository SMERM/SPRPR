# Seminario di Programmazione Professionale (SPRPR) - Incontro del 15/06/2023

### Espressioni regolari

* funzione delle espressioni regolari
* classi di caratteri
* sostituzioni
* gruppi
* concatenazioni
* strumenti che utilizzano le espressioni regolari: `grep`, `sed`, `awk`, ecc.
* le espressioni regolari nei linguaggi di programmazione

<!--
### strumenti di test

  * `unit testing`
  * `rSpec`

### linguaggi di programmazione:

#### interpretati

#### compilati

#### *byte-code* producers

## Tipologie di programmazione

### *Test-driven development*

#### *unit testing*

#### *functional testing*

### *Behaviour-driven development*

### *Fixtures* e *Mock-ups*

## Cicli di programmazione

### programmazione agile

### integrazione continua

## Tecniche di programmazione

### regole generali

### programmazione strutturata

### programmazione a oggetti

## Interfacce utente

## *Single-tier* vs. *Multi-tier* programming

### *Monolithic* programming

### *Multi-task* programming

### *Multi-thread* programming

### *Model-viev-controller* programming

## Programmazione di sistema

### programmazione di *device drivers*

### sistemi fisici vs. sistemi virtuali

### containers

### orchestrators

### strumenti di sequenza (*make* e derivati)

### strumenti di test

### linguaggi di programmazione:

#### interpretati

#### compilati

#### *byte-code* producers

## Tipologie di programmazione

### *Test-driven development*

#### *unit testing*

#### *functional testing*

### *Behaviour-driven development*

### *Fixtures* e *Mock-ups*

## Cicli di programmazione

### programmazione agile

### integrazione continua

## Tecniche di programmazione

### regole generali

### programmazione strutturata

### programmazione a oggetti

## Interfacce utente

## *Single-tier* vs. *Multi-tier* programming

### *Monolithic* programming

### *Multi-task* programming

### *Multi-thread* programming

### *Model-viev-controller* programming

## Programmazione di sistema

### programmazione di *device drivers*

### sistemi fisici vs. sistemi virtuali

### containers

### orchestrators
-->

