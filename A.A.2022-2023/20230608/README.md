# Seminario di Programmazione Professionale (SPRPR) - Incontro del 08/06/2023

### Ripasso degli strumenti di sequenza
  * `make`
  * `cmake`
  * `waf`
  * `ant`
  * `rake`
  * ...

### Esempi di `Makefile`

[./make/Makefile](./make/Makefile)
```make
target : depend
	(cat depend; echo "with depend") > target
depend : source Makefile
	(cat source; echo "with depend") > depend
clean :
	 $(RM) target depend
.PHONY : clean
.SUFFIXES : .smerm .sme

%.smerm: %.sme
	(cat $<; echo "trasformato in .$@") > $@
```

[./make/sub/Makefile](./make/sub/Makefile)
```make
TARGETS= 00.smerm 01.smerm 02.smerm 03.smerm 04.smerm 05.smerm 06.smerm 07.smerm 08.smerm 09.smerm 10.smerm 11.smerm 12.smerm 13.smerm 14.smerm 15.smerm 16.smerm 17.smerm 18.smerm 19.smerm 20.smerm 21.smerm 22.smerm 23.smerm 24.smerm 25.smerm 26.smerm 27.smerm 28.smerm 29.smerm 30.smerm 31.smerm 32.smerm 33.smerm 34.smerm 35.smerm 36.smerm 37.smerm 38.smerm 39.smerm 40.smerm 41.smerm 42.smerm 43.smerm 44.smerm 45.smerm 46.smerm 47.smerm 48.smerm 49.smerm 50.smerm 51.smerm 52.smerm 53.smerm 54.smerm 55.smerm 56.smerm 57.smerm 58.smerm 59.smerm 60.smerm 61.smerm 62.smerm 63.smerm 64.smerm 65.smerm 66.smerm 67.smerm 68.smerm 69.smerm 70.smerm 71.smerm 72.smerm 73.smerm 74.smerm 75.smerm 76.smerm 77.smerm 78.smerm 79.smerm 80.smerm 81.smerm 82.smerm 83.smerm 84.smerm 85.smerm 86.smerm 87.smerm 88.smerm 89.smerm 90.smerm 91.smerm 92.smerm 93.smerm 94.smerm 95.smerm 96.smerm 97.smerm 98.smerm 99.smerm
SOURCES=$(TARGETS:.smerm=.sme)
all : .target
.target : .sources_done 
	$(MAKE) -$(MAKEFLAGS) real_target
	touch .target
real_target : $(TARGETS)
.sources_done : create
	touch .sources_done
realclean : clean
	$(RM) $(SOURCES) .sources_done .target
clean :
	 $(RM) $(TARGETS)
create :
	x=0; while [ $$x -lt 100 ];\
	do\
		name=$$(printf "%02d.sme" $$x);\
		echo $$x > $$name;\
		let x=$$x+1;\
	done
.PHONY : clean create
.SUFFIXES : .smerm .sme

%.smerm: %.sme
	(cat $<; echo "trasformato in $@") > $@
```

(quest'ultimo `Makefile` non funziona come dovrebbe, perché non segue le
dipendenze come dovrebbe)


<!--
### Espressioni regolari

* funzione delle espressioni regolari
* classi di caratteri
* sostituzioni
* gruppi
* concatenazioni
* strumenti che utilizzano le espressioni regolari: `grep`, `sed`, `awk`, ecc.
* le espressioni regolari nei linguaggi di programmazione

### strumenti di test

  * `unit testing`
  * `rSpec`

### linguaggi di programmazione:

#### interpretati

#### compilati

#### *byte-code* producers

## Tipologie di programmazione

### *Test-driven development*

#### *unit testing*

#### *functional testing*

### *Behaviour-driven development*

### *Fixtures* e *Mock-ups*

## Cicli di programmazione

### programmazione agile

### integrazione continua

## Tecniche di programmazione

### regole generali

### programmazione strutturata

### programmazione a oggetti

## Interfacce utente

## *Single-tier* vs. *Multi-tier* programming

### *Monolithic* programming

### *Multi-task* programming

### *Multi-thread* programming

### *Model-viev-controller* programming

## Programmazione di sistema

### programmazione di *device drivers*

### sistemi fisici vs. sistemi virtuali

### containers

### orchestrators

### strumenti di sequenza (*make* e derivati)

### strumenti di test

### linguaggi di programmazione:

#### interpretati

#### compilati

#### *byte-code* producers

## Tipologie di programmazione

### *Test-driven development*

#### *unit testing*

#### *functional testing*

### *Behaviour-driven development*

### *Fixtures* e *Mock-ups*

## Cicli di programmazione

### programmazione agile

### integrazione continua

## Tecniche di programmazione

### regole generali

### programmazione strutturata

### programmazione a oggetti

## Interfacce utente

## *Single-tier* vs. *Multi-tier* programming

### *Monolithic* programming

### *Multi-task* programming

### *Multi-thread* programming

### *Model-viev-controller* programming

## Programmazione di sistema

### programmazione di *device drivers*

### sistemi fisici vs. sistemi virtuali

### containers

### orchestrators
-->

