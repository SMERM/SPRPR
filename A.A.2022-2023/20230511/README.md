# Seminario di Programmazione Professionale (SPRPR) - Incontro del 11/05/2023

## Introduzione

* ecosistema del seminario
* il mercato del software
* strumentario professionale
* linguaggi di programmazione
* tipologie di programmazione
* cicli di programmazione
* tecniche di programmazione
* interfacce utente
* *single-tier* vs. *multi-tier*
* programmazione di sistema

## Ecosistema del seminario

* *git*
* repositories individuali su *gitlab*
* progetti individuali (o di gruppo):
  * *SEC*
  * *osc*
  * *nel*
  * *spotify analyzer*
  * *freesound interface app*
  * *fortelib*
  * ...

## Il mercato del software

* software non-libero
* Software Libero

## Strumentario professionale

### *command-line interface*

#### comandi essenziali

* *ls*
* *grep*
* *cat*
* *more*/*less*
* ...

### strumenti SCM

* introduzione a `git`:
  * inner workings
  * `commt`, `push`, `checkout`, `pull`
  * *branching*
  * *forking*
* [`git_hack`](https://gitlab.com/SMERM/git_hack.git) sandbox repository per fare esercizio

## Lavagna

![whiteboard 1](./SPRPR 1 11-05-2023 16.15.jpg)
